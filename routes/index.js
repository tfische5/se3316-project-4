var express = require('express');
var router  = express.Router();

router.route('/')
	.get(function(req, res) {
		res.render('index', { 
			user : res.locals.user,
			login : false,
			register : false,
			error : 'none'
		});
	});

router.route('/login')
	.get(function(req, res) {
		res.render('index', { 
			user : res.locals.user,
			login : true,
			register : false,
			error : 'none'
		});
	});

router.route('/register')
	.get(function(req, res) {
		res.render('index', { 
			user : res.locals.user,
			login : false,
			register : true,
			error : 'none'
		});
	});

module.exports = router;
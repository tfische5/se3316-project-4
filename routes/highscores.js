var express = require('express');
var router  = express.Router();

router.route('/highscores')
	.get('/highscores', function(req, res) {
		var query = "SELECT users.username, highscores.score, DATE_FORMAT(highscores.scoreDate, '%b %e, %Y') AS date " +
					"FROM highscores " +
					"INNER JOIN users ON highscores.uid = users.uid " +
					"ORDER BY highscores.score DESC " +
					"LIMIT 10";
		connection.query(query, function(err, rows, fields) {
			if (err) {
				console.log(err);
				throw err;
			}
			
			res.json(rows);
		});
	})
	
	.post(function(req, res) {
		console.log(req.body);
		var query = "INSERT INTO highscores (score, uid) VALUES ('" + req.body.score + "', 3)"
		connection.query(query, function(err, rows, fields) {
			if (err) {
				console.log(err);
				return next('MySQL Error: Check your query');
			}
			
			res.json({ message: 'Highscore created!' });
			// res.sendStatus(200);
		});
	});

module.exports = router;
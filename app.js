// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express'); 		// call express
var app        = express(); 				// define our app using express
var bodyParser = require('body-parser');
var index	   = require('./routes/index');
var http 	   = require('http');
var server     = http.Server(app);
var path 	   = require('path');
var validator  = require('express-validator');
var mysql      = require('mysql');
var io         = require('socket.io')(server);
// var stormpath  = require('express-stormpath');
var stormpath  = require('stormpath');

var connection = mysql.createConnection({
	host: '127.0.0.1',
	port: '3306',
	user: 'root',
	password: 'western_u',
	database: 'spaceInvaders',
	debug: false
});

connection.connect(function(err) {
	if (err) {
		console.log(err);
		throw err;
	} else {
		console.log('Connected');
	}
});

app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(validator());

app.use('/public/game/images', express.static(__dirname + '/public/game/images'));
app.use('/public/stylesheets', express.static(__dirname + '/public/stylesheets'));
app.use('/public/images', express.static(__dirname + '/public/images'));
app.use('/public/scripts', express.static(__dirname + '/public/scripts'));

/*
var client = null;
var keyfile = '/home/tomfischer/Downloads/apiKey.properties';

stormpath.loadApiKey(keyFile, function apiKeyFileLoaded(err, apiKey) {
	client = new stormpath.Client({ apiKey: apiKey });
});

var appHref = 'https://api.stormpath.com/v1/accounts/4uVizZACj9EfSzdBfmQLYG';

client.getApplication(appHref, function(err, app) {
if (err) throw err;

console.log(app);
});*/

var client = null;
var loggedInUser = null;
var keyFile = '/home/tomfischer/Downloads/apiKey.properties';
var application = null;

stormpath.loadApiKey(keyFile, function apiKeyFileLoaded(err, apiKey) {
	client = new stormpath.Client({ apiKey: apiKey });
	
	client.getApplications({ name : 'Space Invaders' }, { createDirectory : true }, function(err, applications) {
		if (err) throw err;
		
		application = applications.items[0];
		console.log(application);
	});
});

/*
app.use(stormpath.init(app, {
    application: 'https://api.stormpath.com/v1/applications/4v1NpgJLAf7geP0TphL3zs',
    secretKey: 'iKawqm91ZqPpgGGlCLf1df9MYQNe4J5Fzq1u8JOrLiU',
    registrationView: __dirname + '/views/index.ejs',
    loginView: __dirname + '/views/index.ejs',
    enableRegistration: false,
    enableLogin: false,
    enableLogout: false,
}));*/

app.get('/', function(req, res) {
	res.render('index', { 
		user : loggedInUser,
		login : false,
		register : false,
		error : 'none'
	});
});

app.get('/login', function(req, res) {
	res.render('index', { 
		user : loggedInUser,
		login : true,
		register : false,
		error : 'none'
	});
});

app.get('/register', function(req, res) {
	res.render('index', { 
		user : loggedInUser,
		login : false,
		register : true,
		error : 'none'
	});
});

app.get('/logout', function(req, res) {
	loggedInUser = null;
	
	res.render('index', { 
		user : loggedInUser,
		login : false,
		register : false,
		error : 'none'
	});
});

app.post('/register', function(req, res) {
	var firstName = req.body.firstName;
	var lastName = req.body.lastName;
	var email = req.body.email;
	var password = req.body.password;
	
	if (!firstName || !lastName || !email || !password) {
		res.redirect('/');
		return;
	}
	
	var account = {
	  givenName: firstName,
	  surname: lastName,
	  email: email,
	  password: password
	};
	
	var accountCreated = application.createAccount(account, function(err, createdAccount) {
		if (err)
		{
			res.render('index', { 
				user : loggedInUser,
				login : false,
				register : true,
				error : err
			});
			
			return false;
		}
		
		console.log(createdAccount);
		
		loggedInUser = createdAccount;
		
		res.render('index', { 
			user : loggedInUser,
			login : false,
			register : false,
			error : 'none'
		});
		
		return true;
	});
});

app.post('/login', function(req, res) {
	 application.authenticateAccount({
		 username: req.body.email,
		 password: req.body.password,
	 }, function (err, result) {
		 if (err) throw err;
		 
		 console.log('Successfully authenticated account using email!');
		 loggedInUser = result.account;

		 res.render('index', { 
			user : loggedInUser,
			login : false,
			register : false,
			error : 'none'
		 });
	 }); 
});

app.get('/messages', function(req, res) {
	var query = "SELECT username, message, DATE_FORMAT(messageDate, '%h:%i%p, %e/%c/%Y') AS date " +
				"FROM messages " +
				"ORDER BY messageDate " +
				"LIMIT 100";
	connection.query(query, function(err, rows, fields) {
		if (err) {
			console.log(err);
			throw err;
		}
		
		res.json(rows);
	});
});

app.post('/messages', /*stormpath.loginRequired,*/ function(req, res) {
	console.log(res.locals.user);
	var query = "INSERT INTO messages (message, username) " +
	            "VALUES ('" + req.body.message + "', '" + loggedInUser.fullName + "')"
	connection.query(query, function(err, rows, fields) {
		if (err) {
			console.log(err);
			return next('MySQL Error: Check your query');
		}
		
		res.json({ message: 'Message created!' });
		// res.sendStatus(200);
	});
});

app.get('/highscores', function(req, res) {
	var query = "SELECT username, score, DATE_FORMAT(highscoreDate, '%b %e, %Y') AS date " +
				"FROM highscores " +
				"ORDER BY score DESC " +
				"LIMIT 10";
	connection.query(query, function(err, rows, fields) {
		if (err) {
			console.log(err);
			throw err;
		}
		
		res.json(rows);
	});
});

app.post('/highscores', /*stormpath.loginRequired,*/ function(req, res) {
	var query = "INSERT INTO highscores (score, username) " +
	            "VALUES ('" + req.body.score + "', '" + loggedInUser.fullName + "')"
	connection.query(query, function(err, rows, fields) {
		if (err) {
			console.log(err);
			return next('MySQL Error: Check your query');
		}
		
		res.json({ message: 'Highscore created!' });
	});
});

var ips = {};

app.get('/locations', function(req, res) {
	res.json(ips);
});

io.on('connection', function(socket) {
	socket.on('ehlo', function(data, callback) {
		ips[socket.id] = data;
		
		var client = ips[socket.id];
        console.log("User connected - ID: " + socket.id + ", Latitude: " + client[0] + ", Longitude: " + client[1]);
        
        callback('error', 'message');
    });
	
	socket.on('disconnect', function() {
		var client = ips[socket.id];
		
		if (client) {
			console.log("User disconnected - ID: " + socket.id + ", Latitude: " + client[0] + ", Longitude: " + client[1]);
			delete ips[socket.id];
		}
		//ips.splice();
    });
});

//app.use('/', index);
//app.use('/', auth_routes);
//app.use('/', messages);
//app.use('/', highscores);

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); 				// get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });	
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
server.listen(app.get('port'), function() {
	//console.log(io);
    console.log('Express server listening on port ' + app.get('port'));
});

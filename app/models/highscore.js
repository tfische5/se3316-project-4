var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var HighscoreSchema = new Schema({
	username: String,
	score: Number,
	dateFormat: String,
	date: Date
});

module.exports = mongoose.model('Highscore', HighscoreSchema);
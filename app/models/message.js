var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var MessageSchema = new Schema({
	username: String,
	message: String,
	dateFormat: String,
	date: Date
});

module.exports = mongoose.model('Message', MessageSchema);
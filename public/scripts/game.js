/**
 * New node file
 */
$(document).ready(function() { 
	var game_div = document.getElementById("game");
	game_div.setAttribute("style", "width: 100%");
	
	var game = new Phaser.Game(game_div.clientWidth, 450, Phaser.AUTO, 'game');
	
	var ship;
	var cannon;
	var leftClick;
	var bullets;
	var lives;
	var scoreText;
	var livesText;
	var button;
	var pausePanel;
	var bulletDelay = 0;
	var newAlienCounter = 2;
	var score = 0;
	var started = false;
	
	var main_state = {
	    preload: function() {
	    	game.load.spritesheet('alien_slow', './public/game/images/monster1.png', 32, 32, 2);
	    	game.load.spritesheet('alien_medium', './public/game/images/monster2.png', 44, 32, 2);
	    	game.load.spritesheet('alien_fast', './public/game/images/monster3.png', 48, 32, 2);
	    	
	    	game.load.image('ship_base', './public/game/images/ship_base.png');
	    	game.load.image('ship_cannon', './public/game/images/ship_cannon.png');
	    	game.load.image('ship', './public/game/images/ship.png');
	    	game.load.image('alien_boss', './public/game/images/monster4.png');
	    	game.load.image('bullet', './public/game/images/bullet.png');
	    	game.load.image('button', './public/game/images/button.png');
	    },
	
	    create: function() { 
	    	game.physics.startSystem(Phaser.Physics.ARCADE);
	    	game.world.setBoundsToWorld;
	    	game.stage.backgroundColour = "#000000";
	    	
	    	bullets = game.add.group();
	    	bullets.enableBody = true;
	    	bullets.physicsBodyType = Phaser.Physics.ARCADE;
	    	bullets.createMultiple(5, 'bullet');
	    	bullets.setAll('anchor.x', 0.5);
	    	bullets.setAll('anchor.y', 0.5);
	    	bullets.setAll('checkWorldBounds', true);
	        bullets.setAll('outOfBoundsKill', true);
	        
	        aliens = game.add.group();
	        aliens.enableBody = true;
	        aliens.physicsBodyType = Phaser.Physics.ARCADE;
	        
	        lives = game.add.group();
	        lives.enableBody = true;
	        lives.physicsBodyType = Phaser.Physics.ARCADE;
	        
	    	var livesWidth = game.cache.getImage('ship').width;
	    		        
	        for (var i = 1; i < 6; i++) {
	        	var life = lives.create(game_div.clientWidth - (livesWidth + 5) * i - 15, 25, 'ship');
	        	life.anchor.set(0, 0.5);
	        }
	         	
	    	// game.input.addPointer();
	    	
	    	ship = game.add.sprite(Math.floor(game_div.clientWidth / 2), game_div.clientHeight, 'ship_base');
	    	ship.anchor.set(0.5, 1);
	    	
	    	cannon = game.add.sprite(Math.floor(game_div.clientWidth / 2), game_div.clientHeight - 5, 'ship_cannon');
	    	cannon.animations.add('fly');
	    	cannon.animations.play('fly', 100, true);
	    	cannon.anchor.set(0.5, 1);

	    	game.physics.enable(cannon, Phaser.Physics.ARCADE);
	    	game.physics.enable(ship, Phaser.Physics.ARCADE);
	    	
	    	// button = game.add.button(game.world.centerX - 95, 400, 'button', pauseGame, this, 2, 1, 0);
	    	//pausePanel = new PausePanel(game);
	    	//game.add.existing(pausePanel);

	    	scoreText = game.add.text(20, 20, "Score: " + score, {
	            font: '16px "Space Invaders"',
	            fill: "#ffffff",
	            align: "center"
	        });
	    	
	    	livesText = game.add.text(game_div.clientWidth - (livesWidth + 5) * 6 - 30, 20, "Lives:", {
	            font: '16px "Space Invaders"',
	            fill: "#ffffff",
	            align: "center"
	        });
	    	
	    	introText = game.add.text(game.world.centerX - 85, game.world.centerY, 'Click to start', {
	    		font: '20px "Space Invaders"', 
	    		fill: '#ffffff', 
	    		align: 'center' 
	    	});
	    	
	    	game.pause = true;
	    },
	
	    update: function() {
	    	game.physics.arcade.collide(bullets, aliens, alienShot);
	    	
	    	aliens.forEachAlive(function(alien) {
	    		if (alien.y >= game_div.clientHeight)
	    			alienOutOfBounds(alien);
	    	}); 
	    	
	    	if (game.input.activePointer.isDown) {
	    		start();
	    		
	    		if (game.time.now > bulletDelay && bullets.countDead() > 0) {
	    			bulletDelay = game.time.now + 125;
	    			
	    			var bullet = bullets.getFirstDead();
	    			bullet.rotation = cannon.rotation;
	    			bullet.reset(cannon.body.x + 6, cannon.body.y + 32);
	    			game.physics.arcade.moveToPointer(bullet, 1500);
	    		}
	    	}
	    	  	
	    	cannon.rotation = game.physics.arcade.angleToPointer(cannon) + Math.PI / 2;
	    },
	    
	    render: function() {
	    	// game.debug.spriteInfo(ship, 32, 32);
	    }
	}
	
	function createAlien() {
		var alien = aliens.getFirstExists(false);
		
		if (alien) {
			alien.revive();
			alien.reset(getRandomInt(46, game_div.clientWidth - 46), -46);
		} else {
			alien = aliens.create(getRandomInt(46, game_div.clientWidth - 46), -46);
			alien.antialias = false;
			alien.checkWorldBounds = true;
		}
		
		alien = setAlienSprite(alien);
	}
	
	function alienOutOfBounds(alien) {
		alien.reset(getRandomInt(46, game_div.clientWidth - 46), -96);
		alien.body.velocity.y = 50 + Math.random() * 200;
		
		lives.getFirstAlive().kill();
		
		if (lives.countLiving() <= 0) {
			highscoresPostRequest(score);
			highscoresGetRequest();
			
			alert("You lose! Your score was: " + score);
			
			aliens.forEachAlive(function(alien) {
				alien.kill();
			});

			restart();
		}
	}
	
	function alienShot(bullet, alien) {
		alien.reset(getRandomInt(46, game_div.clientWidth - 46), -46);
		alien = setAlienSprite(alien);
		
		score += 1;
		bullet.kill();
		
		if (score > newAlienCounter) {
			createAlien();
			newAlienCounter = score * 2 + 2
		}
		
		scoreText.setText("Score: " + score);
	}
	
	function restart() {
		score = 0;
		started = false;
		newAlienCounter = 2;
		
		scoreText.setText("Score: " + score);
		
		for (var i = 1; i < 6; i++) {
        	lives.getAt(i - 1).revive();
        }
		
		introText.setText("Click to start");
	}
	
	function getRandomInt(min, max) {
		return n = Math.floor(Math.random() * (max - min + 1)) + min;
	}
	
	function setAlienSprite(alien) {
		var random = 50 + Math.random() * 200;
		
		if (random < 145) {
			alien.loadTexture('alien_slow');
			
			var image = game.cache.getImage('alien_slow');
			alien.body.setSize(image.width, image.height);
		} else if (random < 200) {
			alien.loadTexture('alien_medium');
			
			var image = game.cache.getImage('alien_medium');
			alien.body.setSize(image.width, image.height);
		} else if (random < 240) {
			alien.loadTexture('alien_fast');
			
			var image = game.cache.getImage('alien_fast');
			alien.body.setSize(image.width, image.height);
		} else {
			alien.loadTexture('alien_boss');
			
			var image = game.cache.getImage('alien_boss');
			alien.body.setSize(image.width, image.height);
		}
		
		alien.body.velocity.y = random;
		
		if (random <= 240) {
			alien.animations.add('enemy_fly');
			alien.animations.play('enemy_fly', 5, true);
		}
		
		return alien;
	}
	
	function start() {
		if (!started) {
			started = true;
			introText.setText("");
			createAlien();
		}
	}
	
	game.state.add('main', main_state);  
	game.state.start('main'); 
});
$(document).ready(function () {
	chatGetRequest();
	$('.chatbox').scrollTop($('.chatbox')[0].scrollHeight);

	setInterval(function() { chatGetRequest() }, 1000);
});

$('.chatbox-form').submit(function(e) {
	e.preventDefault();
	
	chatPostRequest();
	chatGetRequest();

	$('#chatbox-message-field').val('');
	$('.chatbox').scrollTop($('.chatbox')[0].scrollHeight);
});

var chatPostRequest = function() {
	$.ajax({
		url: "messages",
        type: "POST",
        dataType: "jsonp",
        jsonp: false,
        cache: true,
        data: { 
        	message: $("#chatbox-message-field").val(),
        }
    });
}

var chatGetRequest = function() {
	$.ajax({
        url: "messages",
        type: "GET",
        dataType: "json",
        cache: true,
        success: function(data) {
		    $("#chatbox-table tr").remove();

			var tbody = $('#chatbox-table').children('tbody');
			var table = tbody.length ? tbody : $('#chatbox-table');

			$.each(data, function(index, element) {
		    	table.append('<tr><td class="chatbox-message-user">' + element.username + 
		    		         ':</td><td>' + element.message + 
		    		         '</td><td class="chatbox-message-time">' + element.date + '</td></tr>');
		    });
		},
        error: function (jqXHR, textStatus, errorThrown) {
            if (typeof errorFn !== 'undefined') {
                errorFn(jqXHR, textStatus, errorThrown);
            }
        }
    });
}
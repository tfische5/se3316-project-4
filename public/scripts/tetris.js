/* --------------------------------------- */
/*               Point Class               */
/* --------------------------------------- */

function Point(x, y) {
    // Constructor
    this.x = x;
    this.y = y;
}

Point.prototype.x = 0;                             // Holds the x coordinate
Point.prototype.y = 0;                             // Holds the y cooddinate

Point.prototype.moveTo = function (x, y) {          // Moves the point to a new location
    this._x = x;
    this._y = y;
}

Point.prototype.toString = function () {            // For debugging purposes
    return "x: " + this._x + ", y: " + this._y;
}

/* --------------------------------------- */
/*                Constants                */
/* --------------------------------------- */

var COLOUR = {
	"WHITE": "#FFFFFF",
	"BLACK": "#000000",
	"BLUE": "#3498db",
    "YELLOW":"#f1c40f",
    "GREEN":"#2ecc71",
    "RED": "#e74c3c",
    "PURPLE": "#9b59b6",
    "ORANGE": "#e67e22",
    "TURQUOISE":"#1abc9c"
}

/*
var COLOUR = {
		"BLUE"     : [0, "#3498db"],
	    "YELLOW"   : [1, "#f1c40f"],
	    "GREEN"    : [2, "#2ecc71"],
	    "RED"      : [3, "#e74c3c"],
	    "PURPLE"   : [4, "#9b59b6"],
	    "ORANGE"   : [5, "#e67e22"],
	    "TURQUOISE": [6, "#1abc9c"],
		"WHITE"    : [7, "#FFFFFF"],
		"BLACK"    : [8, "#000000"]
	}
*/

if (Object.freeze) { Object.freeze(COLOUR); }

/* --------------------------------------- */
/*               Cell Class                */
/* --------------------------------------- */

function Cell(x, y, colour) {
    // Constructor
    Point.call(this, x, y);                         // Square extends Point (Point <- Square)

    this.colour = colour;                           // Holds the state of the square
}

Cell.prototype = new Point();                       // Makes this class it's parent type
Cell.prototype.constructor = Cell;                  // Calls the Square constructor
Cell.prototype.colour = COLOUR.WHITE;

/* --------------------------------------- */
/*               Grid Class                */
/* --------------------------------------- */

function Grid(columns, rows) {
	this.columns = columns
	this.rows = rows;
	
	for (var i = 0; i < this.columns; i++) {
		this.matrix[i] = [];
		
		for (var j = 0; j < this.rows; j++) {
			this.matrix[i][j] = new Cell(i, j, COLOUR.WHITE);
		}
	}
}

Grid.prototype.constructor = Grid;
Grid.prototype.columns = 0;
Grid.prototype.rows = 0;
Grid.prototype.dimensions = 20;
Grid.prototype.matrix = [];

Grid.prototype.drawGrid = function() {
	ctx.fillStyle = COLOUR.BLACK;
	
	for (var i = 0; i < this.columns; i++) {
		for (var j = 0; j < this.rows; j++) {
			ctx.rect(i * this.dimensions, j * this.dimensions, this.dimensions + 0.5, this.dimensions + 0.5);
		}
	}
	
	ctx.stroke();
}

Grid.prototype.drawCells = function() {
	ctx.fillStyle = COLOUR.BLACK;
	
	COLOURS.forEach(function(colour) {
		if (colour !== COLOUR.WHITE && colour !== COLOUR.BLACK) {
			for (var i = 0; i < this.columns; i++) {
				for (var j = 0; j < this.rows; j++) {
					if (grid.matrix[i][j].colour == colour)
						ctx.rect(i * this.dimensions, j * this.dimensions, this.dimensions + 0.5, this.dimensions + 0.5);
				}
			}
		}
	});
	
	ctx.stroke();
}

/* --------------------------------------- */
/*              Shape Class                */
/* --------------------------------------- */

function Shape() {

}

Shape.prototype.constructor = Shape;
Shape.prototype.cells = [];
Shape.prototype.colour = null;
Shape.prototype.locked = false;

Shape.prototype.moveDown = function() {
	this.cells.forEach(function(cell) {
		if (cell.y + 1 >= grid.rows && grid.matrix[cell.x][cell.y + 1].colour !== COLOUR.WHITE) {
			this.locked = true;
			return false;
		}
	});

	this.cells.forEach(function(cell) {
		var colour = cell.colour;
		cell.colour = COLOUR.WHITE;
		ctx.clearRect(cell.x, cell.y, cell.x + grid.dimensions, cell.y + grid.dimensions);
		
		cell.y++;
		cell.colour = colour;
		
		ctx.fillStyle = cell.colour;
	});
	
	ctx.stroke();
	
	return true;
}

Shape.prototype.addToShape = function (x, y, colour) {
	this.cells.push(grid.matrix[x][y]);
	grid.matrix[x][y].colour = colour;
}

/* --------------------------------------- */
/*              Square Class               */
/* --------------------------------------- */

function Square() {
	Shape.call(this);
	
	this.addToShape(6, 2, COLOUR.ORANGE);
	this.addToShape(7, 2, COLOUR.ORANGE);
	this.addToShape(6, 3, COLOUR.ORANGE);
	this.addToShape(7, 3, COLOUR.ORANGE);
}

Square.prototype = new Shape;

/* --------------------------------------- */
/*                Line Class               */
/* --------------------------------------- */

function Line() {
	Shape.call(this);
	
	this.addToShape(6, 0, COLOUR.BLUE);
	this.addToShape(6, 1, COLOUR.BLUE);
	this.addToShape(6, 2, COLOUR.BLUE);
	this.addToShape(6, 3, COLOUR.BLUE);
}

Line.prototype = new Shape;

/* --------------------------------------- */
/*                 S Class                 */
/* --------------------------------------- */

function S() {
	Shape.call(this);
	
	this.addToShape(6, 3, COLOUR.PURPLE);
	this.addToShape(7, 3, COLOUR.PURPLE);
	this.addToShape(7, 2, COLOUR.PURPLE);
	this.addToShape(8, 2, COLOUR.PURPLE);
}

S.prototype = new Shape;

/* --------------------------------------- */
/*                 Z Class                 */
/* --------------------------------------- */

function Z() {
	Shape.call(this);
	
	this.addToShape(6, 2, COLOUR.RED);
	this.addToShape(7, 2, COLOUR.RED);
	this.addToShape(7, 3, COLOUR.RED);
	this.addToShape(8, 3, COLOUR.RED);
}

Z.prototype = new Shape;

/* --------------------------------------- */
/*                 T Class                 */
/* --------------------------------------- */

function T() {
	Shape.call(this);
	
	this.addToShape(6, 2, COLOUR.YELLOW);
	this.addToShape(7, 2, COLOUR.YELLOW);
	this.addToShape(7, 3, COLOUR.YELLOW);
	this.addToShape(8, 2, COLOUR.YELLOW);
}

T.prototype = new Shape;

/* --------------------------------------- */
/*                 L Class                 */
/* --------------------------------------- */

function L() {
	Shape.call(this);
	
	this.addToShape(6, 1, COLOUR.TURQUIOSE);
	this.addToShape(6, 2, COLOUR.TURQUIOSE);
	this.addToShape(6, 3, COLOUR.TURQUIOSE);
	this.addToShape(7, 3, COLOUR.TURQUIOSE);
}

L.prototype = new Shape;

/* --------------------------------------- */
/*                 J Class                 */
/* --------------------------------------- */

function J() {
	Shape.call(this);
	
	this.addToShape(7, 1, COLOUR.GREEN);
	this.addToShape(7, 2, COLOUR.GREEN);
	this.addToShape(7, 3, COLOUR.GREEN);
	this.addToShape(6, 3, COLOUR.GREEN);
}

J.prototype = new Shape;

/////////////////////////////////////////////

var ctx = document.getElementById("ctx").getContext("2d");
var grid = null;
var current = null;
var checkPoint = null;
var speed = null;

function initialize() {
	grid = new Grid(12, 24);
	grid.drawGrid();
	speed = 400;
	
	current = randomShape();
	checkPoint = Date.now();
	
	main();
}

function randomShape() {
	var value = Math.floor(Math.random() * 7) + 1;
	
	switch (value) {
		case 1: 
			return new Square();
			break;
		case 2:
			return new Line();
			break;
		case 3: 
			return new S();
			break;
		case 4:
			return new Z();
			break;
		case 5: 
			return new T();
			break;
		case 6:
			return new L();
			break;
		default:
			return new J();
	}
}

function main() {
	if (Math.abs(checkPoint - Date.now()) > speed) {
		// ctx.save();
	    // ctx.setTransform(1, 0, 0, 1, 0, 0);
		if (!current.moveDown()) {
			current = randomShape();
		}
		
		// alert(current.cells[0].x + " " + current.cells[0].y);
		// ctx.clearRect(0, grid.dimensions * grid.rows + 30.5, 500, -23);
		// ctx.stroke();
	    // ctx.restore();
	}
	
	setTimeout(main, 1000 / 60);
}

window.onload = initialize(document.querySelector('canvas'));
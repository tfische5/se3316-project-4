/* --------------------------------------- */
/*               Point Class               */
/* --------------------------------------- */

function Point(x, y) {
    // Constructor
    this._x = x;
    this._y = y;
}

Point.prototype._x = 0;                             // Holds the x coordinate
Point.prototype._y = 0;                             // Holds the y cooddinate

Point.prototype.getX = function () {                // Retrieves the x coordinate
    return this._x;
}

Point.prototype.getY = function () {                // Retrieves the y coordinate
    return this._y;
}

Point.prototype.moveTo = function (x, y) {          // Moves the point to a new location
    this._x = x;
    this._y = y;
}

Point.prototype.toString = function () {            // For debugging purposes
    return "x: " + this._x + ", y: " + this._y;
}

/* --------------------------------------- */
/*             Point Constants             */
/* --------------------------------------- */

var DIRECTION = {                                   // Defines directional vector constants
    "UP": new Point(0, 1),
    "RIGHT": new Point(1, 0),
    "DOWN": new Point(0, -1),
    "LEFT": new Point(-1, 0)
};

var STATE = {                                       // Defines state contants (Type Point so they can be compared to the directional vector constants above)
    "EMPTY": new Point(0, 0),
    "FOOD": new Point(1, 1)
};

if (Object.freeze) {
    Object.freeze(DIRECTION);
    Object.freeze(STATE);
}

/* --------------------------------------- */
/*              Square Class               */
/* --------------------------------------- */

function Square(x, y, state) {
    // Constructor
    Point.call(this, x, y);                         // Square extends Point (Point <- Square)

    this._state = state;                            // Holds the state of the square
}

Square.prototype = new Point();                     // Makes this class it's parent type
Square.prototype.constructor = Square;              // Calls the Square constructor
Square.prototype._state = 0;

Square.prototype.getState = function () {           // Retrieves the square's state
    return this._state;
}

Square.prototype.setState = function (state) {      // Sets the square's state
    this._state = state;
}

/* --------------------------------------- */
/*                Grid Class               */
/* --------------------------------------- */

function Grid(columns, rows, xOff, yOff) {
    // Constructor
    this.rows = parseInt(rows);
    this.columns = parseInt(columns);
    this._xOff = parseInt(xOff);
    this._yOff = parseInt(yOff);

    // +0.5 or the lines are blurry 
    ctx.rect(0, 0, this.columns * DIMENSIONS + 0.5, this.rows * DIMENSIONS + 0.5);        // Draws the border of the grid
    ctx.stroke();                                                               // Makes the border visible

    for (var i = this._xOff; i < this.columns + this._xOff; i++) {             // Rows
        this._grid[i - this._xOff] = [];

        for (var j = this._yOff; j < this.rows + this._yOff; j++) {            // Columns
            // Make a new square in the grid
            this._grid[i - this._xOff][j - this._yOff] = new Square(i * DIMENSIONS + 1, j * DIMENSIONS + 1, STATE.EMPTY);
        }
    }
}

Grid.prototype.rows = 0;                                                       // Holds the number of rows
Grid.prototype.columns = 0;                                                    // Holds the number of columns
Grid.prototype._xOff = 0;                                                       // The x-offset from P(0, 0) of the canvas
Grid.prototype._yOff = 0;                                                       // The y-offset from P(0, 0) of the canvas
Grid.prototype._grid = [];                                                      // The array that will hold all the squares in the grid

Grid.prototype.draw = function () {
    for (var i = this._xOff; i < this.columns + this._xOff; i++) {             // Rows
        for (var j = this._yOff; j < this.rows + this._yOff; j++) {            // Columns
            ctx.rect(this._grid[i - this._xOff][j - this._yOff].getX(), this._grid[i - this._xOff][j - this._yOff].getY(), DIMENSIONS - 1, DIMENSIONS - 1);
        }
    }
}

Grid.prototype.getState = function (x, y) {                              // Retrieves the state in a particular square
    return this._grid[x][y].getState();
}

Grid.prototype.setState = function (x, y, state) {                       // Sets the state in a particular square
    this._grid[x][y].setState(state);
}

Grid.prototype.get = function (x, y) {                                          // Returns a particular square
    return this._grid[x][y];
}

Grid.prototype.numColumns = function () {                                       // Returns the number of columns in the grid
    return this.columns;
}

Grid.prototype.numRows = function () {                                          // Returns the number of rows in the grid
    return this.rows;
}

Grid.prototype.toString = function () {                                         // For debugging purposes
    return this.columns + " " + this.rows;
}

Grid.prototype.debug = function () {                                            // Debugging mode
    /*
    This is a different way to fill in the squares that are not empty. Each pass through the main(), this method will look at each 
    square in the grid and colour any square that is not empty. This lets you check to see that every square that is not filled in
    actually contains STATE.EMPTY
    */

    ctx.fillStyle = "#999999";

    for (var i = this._xOff; i < this.columns + this._xOff; i++) {
        for (var j = this._yOff; j < this.rows + this._yOff; j++) {
            if (this._grid[i][j].getState() != STATE.EMPTY)                     // Checks to see if this square is empty
                ctx.fillRect(this._grid[i][j].getX(), this._grid[i][j].getY(), DIMENSIONS - 1, DIMENSIONS - 1);
        }
    }
}

/* --------------------------------------- */
/*               Snake Class               */
/* --------------------------------------- */

function Snake(head) {
    // Constructor
    this._snake.push(new Point(head.getX(), head.getY()));                          // Add the head of the snake to the array

    grid.setState(this._snake[0].getX(), this._snake[0].getY(), DIRECTION.RIGHT);   // Set the state where the head is on the grid, and make the snake move right to begin
}

Snake.prototype._snake = [];                                                        // An array to hold the snake

Snake.prototype.move = function () {                                                // This method is used to move the snake
    var x = this._snake[0].getX();
    var y = this._snake[0].getY();
    var tailX = this._snake[this._snake.length - 1].getX();
    var tailY = this._snake[this._snake.length - 1].getY()
    var state = grid.getState(x, y);

    grid.setState(tailX, tailY, STATE.EMPTY);

    if (x + state.getX() > grid.numColumns() - 1) {
        x = -1;
    } else if (x + state.getX() < 0) {
        x = grid.numColumns();
    }

    if (y + state.getY() > grid.numRows() - 1) {
        y = -1;
    } else if (y + state.getY() < 0) {
        y = grid.numRows();
    }

    if (grid.getState(x + state.getX(), y + state.getY()) === STATE.FOOD) {
        if (this.add(state)) {
            do {
                randomX = randInt(0, grid.columns);
                randomY = randInt(0, grid.rows);
            } while (grid.getState(randomX, randomY) != STATE.EMPTY);

            if (speed > 90) {
                speed -= 10;
            }

            score += parseInt(50 + bonus);
            bonus = 100;

            food.set(randomX, randomY);
        }
    } else if (grid.getState(x + state.getX(), y + state.getY()) !== STATE.EMPTY) {
        alert("You Lose! Your score was " + score);
        exit();
    }

    ctx.clearRect(grid.get(tailX, tailY).getX(), grid.get(tailX, tailY).getY(), DIMENSIONS - 1, DIMENSIONS - 1);

    this._snake[this._snake.length - 1].moveTo(x + state.getX(), y + state.getY());
    this._snake.unshift(this._snake.pop());

    grid.setState(x + state.getX(), y + state.getY(), state);
}

Snake.prototype.add = function (direction) {
    var x = this._snake[this._snake.length - 1].getX();
    var y = this._snake[this._snake.length - 1].getY();
    var randomX = 0
    var randomY = 0;

    x -= direction.getX();
    y -= direction.getY();

    if (x > grid.numColumns() - 1) {
        x = 0;
    } else if (x < 0) {
        x = grid.numColumns() - 1;
    }

    if (y > grid.numRows() - 1) {
        y = 0;
    } else if (y < 0) {
        y = grid.numRows() - 1;
    }

    this._snake.push(new Square(x, y, direction));

    return true;
}

Snake.prototype.changeDirection = function (i, direction) {
    grid.setState(this._snake[i].getX(), this._snake[i].getY(), direction);
}

Snake.prototype.headDirection = function () {
    return grid.getState(this._snake[0].getX(), this._snake[0].getY());
}

Snake.prototype.draw = function () {
    ctx.fillStyle = "#555555";

    for (var i = 0; i < this._snake.length; i++) {
        ctx.fillRect(grid.get(this._snake[i].getX(), this._snake[i].getY()).getX(), grid.get(this._snake[i].getX(), this._snake[i].getY()).getY(), DIMENSIONS - 1, DIMENSIONS - 1);
        ctx.fillStyle = "#000000";
    }
}

Snake.prototype.size = function () {
    return this._snake.length;
}

function Food(x, y) {
    Square.call(this, x, y, STATE.FOOD);

    this.set(x, y);
}

Food.prototype = new Square();
Food.prototype.constructor = Food;

Food.prototype.set = function (column, row) {
    this.moveTo(column, row);
    grid.setState(column, row, STATE.FOOD);

    ctx.fillStyle = "#FF0000";
    ctx.fillRect(grid.get(this._x, this._y).getX(), grid.get(this._x, this._y).getY(), DIMENSIONS - 1, DIMENSIONS - 1);
}

/* --------------------------------------- */
/*             Input Handlers              */
/* --------------------------------------- */

function keyDownHandler(e) {
    if (allowed === true) {
        if (e.keyCode == 37 && (snake.headDirection() != DIRECTION.RIGHT && snake.size() > 1 || snake.size() === 1)) {
            snake.changeDirection(0, DIRECTION.LEFT);
        } else if (e.keyCode == 38 && (snake.headDirection() != DIRECTION.UP && snake.size() > 1 || snake.size() === 1)) {
            snake.changeDirection(0, DIRECTION.DOWN);
        } else if (e.keyCode == 39 && (snake.headDirection() != DIRECTION.LEFT && snake.size() > 1 || snake.size() === 1)) {
            snake.changeDirection(0, DIRECTION.RIGHT);
        } else if (e.keyCode == 40 && (snake.headDirection() != DIRECTION.DOWN && snake.size() > 1 || snake.size() === 1)) {
            snake.changeDirection(0, DIRECTION.UP);
        }

        e.preventDefault();
        allowed = false;
    }
}

/* --------------------------------------- */
/*                 Functions               */
/* --------------------------------------- */

function randInt(min, max) {
    return parseInt(Math.random() * (max - min) + min);
}

/* --------------------------------------- */
/*               Main Program              */
/* --------------------------------------- */

var ctx = document.getElementById("ctx").getContext("2d");
ctx.font = "20px Helvetica";

var DIMENSIONS = 20;
var FRAME_RATE = 60;

var bonus = 100;
var score = 0;
var speed = 400;
var allowed = true;

var checkPoint = Date.now();

var grid = null;
var snake = null;
var food = null;

// grid.draw();

function main() {
    ctx.save();
    ctx.setTransform(1, 0, 0, 1, 0, 0);

    document.addEventListener("keydown", keyDownHandler, false);

    if (Math.abs(checkPoint - Date.now()) > speed) {
        snake.move();
        snake.draw();
        //grid.debug();

        checkPoint = Date.now();
        allowed = true;
    }

    if (bonus > 0) {
        bonus -= 0.2;
    } else {
        bonus = 0;
    }

    ctx.clearRect(0, DIMENSIONS * grid.rows + 30.5, 500, -23);

    ctx.fillStyle = "#000000";
    ctx.fillText("Score: " + score, 0, DIMENSIONS * grid.rows + 30);
    ctx.fillText("Bonus: " + parseInt(bonus), 410, DIMENSIONS * grid.rows + 30);
    ctx.fillText("X: " + (food.getX() + 1) + " Y: " + (food.getY() + 1), 200, DIMENSIONS * grid.rows + 30);

    ctx.stroke();
    ctx.restore();
    setTimeout(main, 1000 / FRAME_RATE);
}

function initialize(canvas) {
    canvas.style.width='100%';                                            // Set the width of the canvas to match the width of the parent
    canvas.width  = canvas.offsetWidth;                                   // Gets the width of th canvas in pixels
    canvas.height = canvas.offsetHeight;

    grid = new Grid(Math.floor(canvas.offsetWidth / 20) - 1, Math.floor(canvas.offsetHeight / 20) - 1, 0, 0);   // Calculate how many columns there are and create a new grid
    snake = new Snake(new Point(Math.floor(grid.columns / 2), Math.floor(grid.rows / 2)));
    food = new Food(randInt(0, grid.columns), randInt(0, grid.rows));

    main();
}

window.onload = initialize(document.querySelector('canvas'));
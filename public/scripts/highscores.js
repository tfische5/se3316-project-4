$(document).ready(function () {
	highscoresGetRequest();
});

var highscoresPostRequest = function(score) {
	$.ajax({
		url: "highscores",
        type: "POST",
        dataType: "jsonp",
        jsonp: false,
        cache: true,
        data: { 
        	score: score,
        }
    });
}

var highscoresGetRequest = function() {
	$.ajax({
        url: "highscores",
        type: "GET",
        dataType: "json",
        cache: true,
        success: function(data) {
        	$("#highscore-table tr:not(:first)").remove();

			var tbody = $('#highscore-table').children('tbody');
			var table = tbody.length ? tbody : $('#highscore-table');

			$.each(data, function(index, element) {
		    	table.append('<tr><td>' + element.username + 
		    		         ':</td><td>' + element.score + 
		    		         '</td><td>' + element.date + '</td></tr>');
		    });
		},
        error: function (jqXHR, textStatus, errorThrown) {
            if (typeof errorFn !== 'undefined') {
                errorFn(jqXHR, textStatus, errorThrown);
            }
        }
    });
}
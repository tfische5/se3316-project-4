$(document).ready(function() {
	var mapOptions = {
      zoom: 5,
      center: new google.maps.LatLng(43.0083, -81.2719),
      mapTypeId: google.maps.MapTypeId.SATELLITE,
      scrollwheel: false
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
	var socket = io();

	socket.on('connect',function(){ 
		getLocation();
	});

	function getLocation() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(showPosition);
	    } else { 
	        x.innerHTML = "Geolocation is not supported by this browser.";
	    }
	}

	function showPosition(position) {
	    var location = [position.coords.latitude, position.coords.longitude];
	    
	    socket.emit('ehlo', location, function(error, message) {
	    	initialize();
	    });
	}
});

function initialize() {
  var heatmap;

  var pointArray = new google.maps.MVCArray(mapGetRequest());

  heatmap = new google.maps.visualization.HeatmapLayer({
    data: pointArray
  });

  heatmap.setMap(map);
}


function changeGradient() {
  var gradient = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 191, 255, 1)',
    'rgba(0, 127, 255, 1)',
    'rgba(0, 63, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(0, 0, 223, 1)',
    'rgba(0, 0, 191, 1)',
    'rgba(0, 0, 159, 1)',
    'rgba(0, 0, 127, 1)',
    'rgba(63, 0, 91, 1)',
    'rgba(127, 0, 63, 1)',
    'rgba(191, 0, 31, 1)',
    'rgba(255, 0, 0, 1)'
  ]
  
  heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
}

function toggleHeatmap() {
  heatmap.setMap(heatmap.getMap() ? null : map);
}

function mapGetRequest() {
	var points = [];
	
	$.ajax({
        url: "locations",
        type: "GET",
        dataType: "json",
        cache: true,
        success: function(data) {
        	for (var key in data) {
        		points.push(new google.maps.LatLng(data[key][0], data[key][1]));
        	}
		},
        error: function (jqXHR, textStatus, errorThrown) {
            if (typeof errorFn !== 'undefined') {
                errorFn(jqXHR, textStatus, errorThrown);
            }
        }
    });
	
	return points;
}